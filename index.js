const express = require('express');

const api = express();

api.get('/', (req, res) => {

    res.json({ env: process.env })

});

api.listen(8080, ()=>{
    console.log("Ready!");
});